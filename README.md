## Getting Started

`settings.py`

```python
CELERY_RESULT_BACKEND = 'django-db'
CELERY_TRACK_STARTED = True
CELERY_TASK_ALWAYS_EAGER = DEBUG

INSTALLED_APPS = [
    # ... other installed apps ...
    'django_celery_beat',
    'django_celery_results',
    'django_celery_beat_plus',
]
```

Activate!

```bash
python manage.py migrate
# (or)
django-admin migrate
# (or)
python -m django migrate
```

## Ways to Execute Tasks

*  Cronjob
*  Django Admin UI Button
*  Django Admin Command
*  Context Processor (injects a message into the person who kicked off the job when jobs run)
*  Middleware

#### Cronjob

**When to use**: Have a cron job (Linux/Mac only) run

#### Django Admin UI Button

**When to use**: If you want to manually kick off tasks

#### Django Admin Command

**When to use**: If you want to initiate jobs from the command-line

#### Context Processor

**When to use**: Great for development! This is great for using a user's navigation to execute new tasks.  It also
has the added benefit of notifying the lucky user who's visit kicked off a job that a job was kicked off.

**How to use**:

In your `settings.py` file:

```python
TEMPLATES = [
    {
        # ... whatever is up here can be included ...
        'OPTIONS': {
            'context_processors': [
                # ... other context processors ...
                'django_celery_beat_plus.context_processors.crontab_processor',
            ],
        },
    },
]
```

OR if you want to ensure that this is only enabled when you're 'developing':

`settings.py` after `TEMPLATES` have been defined
```
import sys
DEVELOPING = sys.argv[1:2] == ['runserver']

if DEVELOPING:
    TEMPLATES[0]['OPTIONS']['context_processors'].append('django_celery_beat_plus.context_processors.crontab_processor')
```

#### Middleware:

**When to use**: Great if you want to use users as your cron check. Essentially the context processor without the messages.

**How to use**:

In your `settings.py` file:

```python
MIDDLEWARE = [
    # ... other middleware ...
    'django_celery_beat_plus.middleware.CrontabMiddleware',
]
```

OR if you want to ensure that this is only enabled when you're 'developing':

`settings.py` after `MIDDLEWARE` have been defined
```
import sys
DEVELOPING = sys.argv[1:2] == ['runserver']
if DEVELOPING:
    MIDDLEWARE += ['django_celery_beat_plus.middleware.CrontabMiddleware']
```

## Extra Information

#### Relevant Windows Stuff

* [Manually install `ephem` module](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyephem)
* [Does Celery support windows?](http://docs.celeryproject.org/en/latest/faq.html#does-celery-support-windows)

#### Relevant Celery Source Code

* [Task Implementation](https://github.com/celery/celery/blob/master/celery/app/task.py)
* [Celery Task](https://github.com/celery/celery/blob/master/celery/app/task.py)
* [Celery Results Manager](https://github.com/celery/django-celery-results/blob/v1.0.1/django_celery_results/managers.py)
* [Traceback](https://docs.python.org/3/library/traceback.html)

#### FAQ

When do results expire?

*  For Database-backed results, they only expire if beat is running.
*  For Redis, AMQP, Cache, the results should automatically expire (verified with Redis)
*  See docs: [result expires](http://docs.celeryproject.org/en/latest/userguide/configuration.html#result-expires)


#### Useful Commands

```bash
# list the workers
sudo su celery -c "DJANGO_SETTINGS_MODULE=www.settings .venv/bin/celery -A www inspect active_queues"
# Show all of the keys
redis-cli keys \*
```

```python
from www import celery_app
i = celery_app.control.inspect()
i.registered()
celery_app.control.ping()
celery_app.control.active()
```

Calling tasks:

```python
# After an import
from tasks import add
add.apply(args=(2,2), kwargs={'kwarg': 2})
add.apply_async(args=(2,2), kwargs={'kwarg': 2})
add.delay(2,2, kwarg=2)

# Directly from Celery's registry
from celery import signature
signature('tasks.add', args=(2, 2), kwargs={'kwargs': 2})
```

sources: [Calling](http://docs.celeryproject.org/en/latest/userguide/calling.html) | [Canvas](http://docs.celeryproject.org/en/latest/userguide/canvas.html#guide-canvas)
