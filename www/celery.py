# import absolute imports from the future, so that our
# celery.py module won’t clash with the library
from __future__ import absolute_import, unicode_literals
import os

from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'www.settings')

# instance of the library, you can have many instances but
# there’s probably no reason for that when using Django
app = Celery('www')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# a common practice for reusable apps is to define all tasks in a
# separate tasks.py module, and Celery does have a way to
# auto-discover these modules
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    return 'Request: {0!r}'.format(self.request)
