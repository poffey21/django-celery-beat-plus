from importlib import import_module
from threading import Thread

from django.conf import settings
from www import celery_app


def eager_thread(function):
    """ Run the delay method as a thread if task is eager """

    def delay(self, *args, **kwargs):
        print('using delay method')
        if self.bind:
            args = [self] + list(args)[:]
        task = Thread(target=self.func, args=args, kwargs=kwargs)
        task.start()
        task.id = task.ident
        return task

    def _wrapper(*args, **kwargs):
        # print('function', function)
        function.delay = delay
        # print('args', args)
        # print('kwargs', kwargs)
        function(*args, **kwargs)
    return _wrapper

# def shared_task(*args, **kwargs):
#     """ Provide folks with the ability to
#
#     This can be used by library authors to create tasks that'll work
#     for any app environment.
#     """
#     # print(__file__)
#     # print('args:', args)
#     # print('kwargs:', kwargs)
#
#     def create_shared_task(**options):
#         # print('options:', options)
#         def __inner(fun):
#             # print('function:', fun)
#             # name = options.get('name')
#             name = 'django_celery_beat.tasks.waiting'
#             bind = options.get('bind', False)
#             task = Task(fun, bind=bind, name=name)
#             celery_app.register_task(task)
#             return task
#         return __inner
#
#     if len(args) == 1 and callable(args[0]):
#         return create_shared_task(**kwargs)(args[0])
#     print('creating a shared task.')
#     new_task = create_shared_task(**kwargs)
#     print(celery_app.tasks)
#     print(dir(new_task))
#     # celery_app.register_task(new_task)
#     return new_task


class Task:
    """
    Task that is used below
    """

    def __init__(self, func, name, bind=False):
        super().__init__()
        self.func = func
        self.bind = bind
        # self.name = name
        self.request = {}

    def delay(self, *args, **kwargs):
        if self.bind:
            args = [self] + list(args)[:]
        task = Thread(target=self.func, args=args, kwargs=kwargs)
        task.start()
        task.id = task.ident
        return task


class Celery:

    def __init__(self, app):
        self.app = app
        self.tasks = {}

    def config_from_object(self, *args, **kwargs):
        """
        Given that we won't really do much... let's ignore config
        :return:
        """



    def autodiscover_tasks(self):
        """
        search for all of the tasks in tasks.py files
        """
        # def discover():
        tasks = {}

        for app in settings.INSTALLED_APPS:
            print(app)
            try:
                task_module = import_module(f'{app}.tasks')
                task_list = [
                    o for o in dir(task_module)
                    if callable(getattr(task_module, o))
                       # and hasattr(getattr(task_module, o.__name__), 'delay')
                ]
                for task in task_list:
                    tasks[f'{app}.tasks.{task}'] = ''
            except ImportError:
                pass
        self.tasks = tasks
        print(self.tasks)

    def task(self, *args, **kwargs):
        return shared_task(*args, **kwargs)
