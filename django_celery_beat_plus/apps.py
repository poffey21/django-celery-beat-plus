"""Django Application configuration."""
from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _

__all__ = ['BeatPlusConfig']


class BeatPlusConfig(AppConfig):
    """Default configuration for django_celery_beat app."""

    name = 'django_celery_beat_plus'
    label = 'django_celery_beat_plus'
    verbose_name = _('Periodic Tasks Plus')
