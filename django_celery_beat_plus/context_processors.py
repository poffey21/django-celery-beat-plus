from django.contrib import messages

from django_celery_beat.models import PeriodicTask
from django_celery_beat_plus.models import Beat


def crontab_processor(request):
    count = Beat.objects.start_next(threaded=True)
    if count:
        if count > 1:
            messages.add_message(request, messages.INFO, f'Started {count} {PeriodicTask._meta.verbose_name_plural}')
        elif count == 1:
            messages.add_message(request, messages.INFO, f'Started {count} {PeriodicTask._meta.verbose_name}')
    return {}
