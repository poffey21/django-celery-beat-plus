import json

from django.core.management import BaseCommand

from django_celery_beat.models import PeriodicTask
from django_celery_beat.models import IntervalSchedule
import logging
logger =logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'adds a beat job to purge logs'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('purge_after_amount', nargs='?', type=int, default=1)
        parser.add_argument('purge_after_period', nargs='?', type=str, default='minutes')
        parser.add_argument('run_every_amount', nargs='?', type=int, default=60)
        parser.add_argument('run_every_period', nargs='?', type=str, default='minutes')

    def handle(self, *args, **options):
        logger.info('Installing a celery results purge scheduled task')
        interval, _ = IntervalSchedule.objects.get_or_create(
            every=options['run_every_amount'],
            period=options['run_every_period'],
        )
        PeriodicTask.objects.update_or_create(
            name='Purge Celery Results',
            defaults=dict(
                task='django_celery_beat_plus.tasks.purge_results',
                interval=interval,
                kwargs=json.dumps(dict(
                    period=options['purge_after_period'],
                    amount=options['purge_after_amount'],
                ))
            )
        )
        logger.info('Successfully installed task.')
        exit(0)
