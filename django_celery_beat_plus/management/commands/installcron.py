import logging

from django.core.management import BaseCommand, CommandError

from django_celery_beat_plus import utils
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'command line utility to setup celery related stuff'

    def handle(self, *args, **options):
        logger.info('Attempting to install .')
        job = utils.is_crontab_configured(True)
        if not job:
            logger.error('Unable to install cronjob.')
            raise CommandError(
                'Unsuccessful attempt to install cron job'
            )
        logger.info('Cron successfully installed.')
