from django.core.management import BaseCommand

from django_celery_beat_plus.models import Beat


class Command(BaseCommand):
    help = 'command line utility used by cron to start jobs'

    def handle(self, *args, **options):
        Beat.objects.start_next()
