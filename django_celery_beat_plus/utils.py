import logging
import sys
import os

from django.conf import settings
from django.core.management import BaseCommand
from django.core.management import load_command_class
from django.middleware.csrf import get_token, _unsalt_cipher_token, _get_failure_view, REASON_BAD_TOKEN
from .tasks import _get_crontab, _update_crontab
from .models import Beat

# Never change - used to detect if created by app or not
SCHEDULE = '* * * * *'
COMMENT = 'Configured by django_celery_beat_plus to run celery tasks on an interval'
DJANGO_ADMIN_COMMAND = 'django-admin'
COMMAND_NAME = 'beat'
SETTINGS_VARIABLE = 'DJANGO_SETTINGS_MODULE'

logger = logging.getLogger(__name__)
info = logger.info
error = logger.error

class CrontabMisconfigured(BaseException):
    """
    Raise this exception when the WSGI OS User is
    configured to run beat Django Admin command.
    """


def _define_command():
    ignore_output = '> /dev/null 2>&1'
    working_directory = settings.BASE_DIR
    # settings
    django_settings_module = os.environ[SETTINGS_VARIABLE]
    python_env = os.path.dirname(sys.executable)
    django_admin = os.path.join(python_env, DJANGO_ADMIN_COMMAND)
    command_class = load_command_class(Beat._meta.app_label, COMMAND_NAME)
    if not isinstance(command_class, BaseCommand):
        raise ModuleNotFoundError('Not a valid Django Command')
    command = (
        f'cd {working_directory} && '
        f'PYTHONPATH=.:$PYTHONPATH '
        f'{SETTINGS_VARIABLE}={django_settings_module} '
        f'{django_admin} {COMMAND_NAME} {ignore_output}'
    )
    return command


def _fix_cron_job(crontab, command, comment, schedule, job=None):
    if job is None:
        info('Installing new cron job')
        job = crontab.new(command=command, comment=comment)
        job.setall(schedule)
        crontab.write()
    else:
        info('Updating job')
        job.setall(schedule)
        job.command = command
        crontab.write()
    return job


def _detect_job_in_crontab(crontab, command, comment, schedule, auto_remediate=False):
    """
    Don't worry about the queue if both are
    running under the same OS User
    """
    final_job = None
    info('Attempting to detect if job is setup in crontab')
    for job in crontab:
        comments_match = job.comment == comment
        commands_match = job.command == command
        schedules_match = job.slices.clean_render() == schedule
        if comments_match or commands_match:
            info('Comments Match: {}'.format(comments_match))
            info('Commands Match: {}: ({}) vs ({})'.format(commands_match, command, job.command))
            if not final_job:
                if comments_match:
                    info('Comments match')
                    if commands_match and schedules_match:
                        # good - but keep looking for others
                        final_job = job
                        continue
                    elif auto_remediate:
                        final_job = _fix_cron_job(crontab, command, comment, schedule, job=job)
                        continue
            if final_job:
                raise CrontabMisconfigured('Duplicate entries appear in crontab')
            else:
                raise CrontabMisconfigured('Crontab is misconfigured')
    if not final_job and auto_remediate:
        info('Attempting to setup a cron job')
        final_job = _fix_cron_job(crontab, command, comment, schedule)
    return final_job


class PhantomTask(BaseException):
    """
    Occurs when a celery task has gone missing.
    """


def is_crontab_configured(configure_if_unset=False):
    info('Initiating an Asynchronous call via Celery')
    queue = _get_crontab.apply_async()
    info('Initiating a Synchronous call via celery')
    web = _get_crontab.apply()
    info('Getting data back from the Async celery call')
    queue.get()

    try:
        command = _define_command()
    except ModuleNotFoundError:
        raise PhantomTask('The beat admin command appears to be missing')

    comment = COMMENT
    schedule = SCHEDULE

    args = (command, comment, schedule)
    kwargs = dict(auto_remediate=configure_if_unset)

    if web.result['user'] != queue.result['user']:
        web_job = _update_crontab.apply(args=args)
        if web_job.result:
            error('Web Job is setup: {}'.format(web_job))
            raise CrontabMisconfigured('The WSGI User (distinct from Celery User) has the job setup.')

    celery_job = _update_crontab.apply_async(args=args, kwargs=kwargs)
    celery_job.get()
    if celery_job.result:
        return True
    return False


def _check_csrf(request, csrf_token):
    reason = REASON_BAD_TOKEN
    csrf_token_from_get = get_token(request)
    try:
        unsalted_token_from_get = _unsalt_cipher_token(csrf_token_from_get)
        unsalted_token_from_request = _unsalt_cipher_token(csrf_token)
        matching = unsalted_token_from_request == unsalted_token_from_get
    except TypeError:
        matching = False
    if not matching:
        logger.warning(
            'Forbidden (%s): %s', reason, request.path,
            extra={
                'status_code': 403,
                'request': request,
            }
        )
        return _get_failure_view()(request, reason=reason)


def csrf_lockdown(func):
    def _wrapper(self, request, *args, **kwargs):
        csrf_token = request.GET.get('v', 'bad token')
        response = _check_csrf(request, csrf_token)
        if response:
            return response
        return func(self, request, csrf_token=get_token(request), *args, **kwargs)
    return _wrapper
