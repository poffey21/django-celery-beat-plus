import json
import logging
import time
from datetime import timedelta
from getpass import getuser

from celery import shared_task
from crontab import CronTab
from django.utils import timezone
try:
    from django_celery_results.models import TaskResult
except (ImportError, RuntimeError) as e:
    TaskResult = None


logger = logging.getLogger(__name__)
info = logger.info


@shared_task(bind=True)
def waiting(self, seconds):
    output = ''
    output += str(timezone.localtime(timezone.now()))
    output += '\n******************************************Sleeping\n'
    output += ', '.join(dir(self))
    time.sleep(seconds)
    output += '\n******************************************Waking Up\n'
    output += str(timezone.localtime(timezone.now()))
    return output


@shared_task
def _get_crontab():
    """
    As the user that runs celery, determine
    if the beat command has been added.
    """
    try:
        crontab = CronTab(user=getuser())
    except FileNotFoundError as e:
        # raised when we're on windows
        crontab = CronTab(tab="""* * * * * command""")
    data = {
        'user': crontab.user,
    }
    return data


@shared_task
def purge_results(amount=None, period=None):
    """Purge the celery results database"""
    period = period or 'seconds'
    amount = amount or 3600
    delta = timedelta(**{period: period})
    if TaskResult is not None:
        count = TaskResult.objects.get_all_expired(delta).count()
        TaskResult.objects.delete_expired(delta)
    else:
        count = 0
    response = {
        'deleted_count': count,
        'expiry_info': {
            'period': period,
            'amount': amount
        }
    }
    return json.dumps(response)


@shared_task
def _update_crontab(command, comment, schedule, auto_remediate=False):
    """
    Don't worry about the queue if both are
    running under the same OS User
    """
    from django_celery_beat_plus.utils import _detect_job_in_crontab
    try:
        crontab = CronTab(user=getuser())
    except FileNotFoundError as e:
        # raised when we're on windows
        crontab = CronTab(tab="""* * * * * command""")
    job = _detect_job_in_crontab(crontab, command, comment, schedule, auto_remediate)
    details = {}
    if job:
        details = {
            'comment': job.comment,
            'command': job.command,
            'schedule': job.slices.clean_render()
        }
    return details
