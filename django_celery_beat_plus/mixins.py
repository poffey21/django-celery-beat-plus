from django.contrib import messages
from django.http import HttpResponseRedirect
from django.middleware.csrf import get_token, _unsalt_cipher_token, _get_failure_view, REASON_BAD_TOKEN
import logging

logger = logging.getLogger(__name__)


class CsrfGetRequestMixin(object):
    def check_csrf(self, csrf_token):
        csrf_token_from_get = get_token(self.request)
        if _unsalt_cipher_token(csrf_token) != _unsalt_cipher_token(csrf_token_from_get):
            # raise PermissionException('Invalid CSRF Token')  # do what you need to do here
            reason = REASON_BAD_TOKEN
            logger.warning(
                'Forbidden (%s): %s', reason, self.request.path,
                extra={
                    'status_code': 403,
                    'request': self.request,
                }
            )
            return _get_failure_view()(self.request, reason=reason)

    def get(self, *args, **kwargs):
        # http://stackoverflow.com/questions/17475324/django-deleteview-without-confirmation-template
        csrf_token = self.request.GET.get('v', None)
        csrf_response = self.check_csrf(csrf_token)
        if csrf_response:
            return csrf_response
        return self.post(*args, **kwargs)

    def get_form_kwargs(self):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = super(CsrfGetRequestMixin, self).get_form_kwargs()
        kwargs.update({
            'data': self.request.GET,
        })
        return kwargs

    def form_invalid(self, form):
        for section, error in form.errors.items():
            messages.add_message(self.request, messages.ERROR, error)
        return HttpResponseRedirect(self.get_success_url())

    def form_valid(self, form):
        response = super(CsrfGetRequestMixin, self).form_valid(form)
        return response
