from time import strftime

from django.contrib import admin, messages
from django.db.models import BooleanField
from django.shortcuts import redirect
from django.urls import path, reverse_lazy
from django.utils import timezone
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from django_celery_beat import admin as beat_admin
from django_celery_beat import models as beat_models

from django_celery_beat_plus.utils import csrf_lockdown, CrontabMisconfigured
from . import models
from . import utils


class BeatInline(admin.StackedInline):
    """
    enable the run once model
    """
    model = models.Beat
    fields = ['run_once']
    template = 'admin/django_celery_beat_plus/snippet_beat_inline.html'


class ScheduleAdmin(admin.ModelAdmin):
    """ rename the model verbose name """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.opts.verbose_name_plural = 'Schedule: %s' % self.opts.verbose_name_plural.capitalize()


class PeriodicTaskAdmin(beat_admin.PeriodicTaskAdmin):
    """ Overrides for Periodic Task"""

    list_display = ['task_name', 'enabled', '_last_run_at', 'next_run_at']
    readonly_fields = ['next_run_at', ]
    change_list_template = 'admin/django_celery_beat_plus/change_list_beat.html'
    inlines = [BeatInline, ]

    def task_name(self, obj):
        name = obj.__str__()
        if obj.beat and obj.beat.run_once:
            if len(name.split(': every')) == 2:
                name = f'{name}'.replace(': every', ': in the next')
            name += ' (run once)'
        return name
    task_name.short_description = 'Periodic Task'

    def next_run_at(self, obj):
        return timezone.localtime(obj.beat.next_run_at)
    next_run_at.short_description = 'When the next job will kick off'

    def _last_run_at(self, obj):
        lra = obj.beat.last_run_at
        return lra
    _last_run_at.short_description = 'Previous job start time'
    _last_run_at.allow_tags = True

    def last_status(self, obj):
        """
        TODO: Actually implement
        """
        return format_html('<img src="/static/admin/img/icon-yes.svg" alt="True">')
    _last_run_at.short_description = 'Last Status'
    _last_run_at.allow_tags = True

    def get_urls(self):
        urls = [
            path(
                'start/', self.admin_site.admin_view(self.refresh_view),
                name='django_celery_beat_plus_beat_start'
            ),
            path(
                'check/', self.admin_site.admin_view(self.crontab_check_view),
                name='django_celery_beat_plus_beat_check'
            ),
            path(
                'update/', self.admin_site.admin_view(self.crontab_update_view),
                name='django_celery_beat_plus_beat_update'
            ),
        ] + super().get_urls()
        return urls

    @csrf_lockdown
    def refresh_view(self, request, *args, **kwargs):
        count = models.Beat.objects.start_next()
        if 1 > count > 1:
            self.message_user(request, f'Starting {count} {self.model._meta.verbose_name_plural}')
        else:
            self.message_user(request, f'Starting {count} {self.model._meta.verbose_name}')
        return redirect(request.META.get('HTTP_REFERER'))

    @csrf_lockdown
    def crontab_check_view(self, request, csrf_token, *args, **kwargs):
        try:
            if utils.is_crontab_configured():
                self.message_user(request, f'Crontab is successfully configured.')
            else:
                url = reverse_lazy('admin:django_celery_beat_plus_beat_update') + f'?v={csrf_token}'
                self.message_user(
                    request,
                    mark_safe(f'Crontab is NOT configured! <a href="{url}">Setup Crontab</a>'),
                    level=messages.ERROR
                )
        except CrontabMisconfigured as e:
            self.message_user(
                request,
                mark_safe(f'Crontab cannot be configured! {e.__str__()}'),
                level=messages.ERROR
            )
        return redirect(request.META.get('HTTP_REFERER'))

    @csrf_lockdown
    def crontab_update_view(self, request, *args, **kwargs):
        job = utils.is_crontab_configured(True)
        if job:
            self.message_user(request, f'Crontab is successfully configured. (Linux/Mac only)')
        else:
            self.message_user(request, f'Crontab is still NOT configured!', level=messages.ERROR)
        return redirect(request.META.get('HTTP_REFERER'))


admin.site.unregister(beat_models.PeriodicTask)
admin.site.unregister(beat_models.SolarSchedule)
admin.site.unregister(beat_models.IntervalSchedule)
admin.site.unregister(beat_models.CrontabSchedule)
admin.site.register(beat_models.PeriodicTask, PeriodicTaskAdmin)
admin.site.register(beat_models.CrontabSchedule, ScheduleAdmin)
admin.site.register(beat_models.IntervalSchedule, ScheduleAdmin)
admin.site.register(beat_models.SolarSchedule, ScheduleAdmin)
