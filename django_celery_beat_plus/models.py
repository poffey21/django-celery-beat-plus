import json
import logging
import traceback
from datetime import datetime, timedelta
from importlib import import_module

import os

import sys
from threading import Thread

from celery import states
from celery.result import EagerResult
from django.apps import apps
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_save
from django.utils import timezone
from django_celery_beat.models import CrontabSchedule
from django_celery_beat.models import IntervalSchedule
from django_celery_beat.models import PeriodicTask
from django_celery_beat.models import SolarSchedule
from django.utils.translation import ugettext_lazy as _

try:
    from django_celery_results.models import TaskResult
except (ImportError, RuntimeError) as e:
    TaskResult = None

logger = logging.getLogger(__name__)


class BeatQuerySet(models.QuerySet):
    """
    Used as the manager for Beat Model
    """

    def need_run_count(self):
        return self.filter(
            next_run_at__isnull=False
        ).filter(
            next_run_at__lte=timezone.now()
        ).count()

    def start_next(self, threaded=False):
        for run in self.filter(next_run_at__isnull=True):
            run.set_next_run()

        jobs_to_run = self.filter(
            next_run_at__lte=timezone.now()
        )
        count = jobs_to_run.count()
        errors = []

        for run in jobs_to_run:
            if threaded:
                t = Thread(target=run.run)
                t.start()
            else:
                run.run()
        return count


class Beat(models.Model):
    """
    Provides a quick way to setup beat without having to run
    the beat celery service.
    """

    periodic_task = models.OneToOneField(PeriodicTask, on_delete=models.CASCADE, null=True)
    next_run_at = models.DateTimeField(blank=True, null=True)
    run_once = models.BooleanField(default=False)
    last_task_id = models.CharField(max_length=255, blank=True)
    _last_run_at = models.DateTimeField(blank=True, null=True, editable=False)

    objects = BeatQuerySet.as_manager()

    @property
    def last_run_at(self):
        periodic_task = self.periodic_task.last_run_at
        if periodic_task is None or periodic_task < self._last_run_at:
            return self._last_run_at
        return periodic_task

    @property
    def total_run_count(self):
        return self.periodic_task.total_run_count

    def validate_unique(self, *args, **kwargs):
        super().validate_unique(*args, **kwargs)

    def set_next_run(self, last_run_at=None, save=True):
        last_run_at = timezone.now() if last_run_at is None else last_run_at
        current_run_at = self.next_run_at
        if self.periodic_task and self.periodic_task.enabled:
            seconds = self.periodic_task.schedule.is_due(last_run_at).next
            now = datetime.now()
            d = now + timedelta(seconds=seconds)
            next_run_at = datetime(d.year, d.month, d.day, d.hour, d.minute, tzinfo=timezone.utc)
            self.next_run_at = next_run_at
        else:
            self.next_run_at = None
        if save and current_run_at != self.next_run_at:
            self.save()

    def run(self):
        start_time = timezone.now()
        module_name = os.path.splitext(self.periodic_task.task)[0]
        task_name = os.path.splitext(self.periodic_task.task)[1].lstrip('.')
        m = import_module(module_name)
        args = json.loads(self.periodic_task.args)
        kwargs = json.loads(self.periodic_task.kwargs)

        self._last_run_at = start_time
        task = None
        try:
            func = getattr(m, task_name)
            self.set_next_run(last_run_at=start_time, save=False)
            self.save()
            task = func.delay(*args, **kwargs)
        except (AttributeError, TypeError, KeyError):
            from kombu.utils.uuid import uuid
            exc_type, exc_value, exc_traceback = sys.exc_info()
            # task no longer available
            if TaskResult is not None:
                TaskResult.objects.store_result(
                    content_type='text/json',
                    content_encoding='utf-8',
                    task_id=uuid(),
                    result='{}',
                    status=states.FAILURE,
                    traceback=traceback.format_tb(exc_traceback),
                )
        if TaskResult is not None and task and isinstance(task, EagerResult):
            TaskResult.objects.store_result(
                content_type='text',
                content_encoding='utf-8',
                task_id=task.id,
                result=task.result,
                status=task.status,
                traceback=None,
            )
        if self.run_once:
            self.periodic_task.enabled = False
            self.periodic_task.save()
        self.save()

    @classmethod
    def save_beat_for_task(cls, instance, **kwargs):
        try:
            beat = instance.beat
            beat.save()
        except ObjectDoesNotExist:
            beat = cls.objects.create(
                periodic_task=instance,
            )
        beat.set_next_run()

    @classmethod
    def save_beat_for_schedule(cls, instance, **kwargs):
        for task in instance.periodictask_set.all():
            cls.save_beat_for_task(task)


post_save.connect(Beat.save_beat_for_task, sender=PeriodicTask)
post_save.connect(Beat.save_beat_for_schedule, sender=IntervalSchedule)
post_save.connect(Beat.save_beat_for_schedule, sender=CrontabSchedule)
post_save.connect(Beat.save_beat_for_schedule, sender=SolarSchedule)
